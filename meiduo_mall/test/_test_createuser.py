#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.prod'
import django
django.setup()

if __name__ == '__main__':
    from users.models import User
    # User.objects.create(username='t4', password='t4', mobile='4')

    from django.utils import six, timezone
    t = timezone.now()
    new_t = timezone.localtime(t)
    print(t)
    print(new_t)

    print('ok')
