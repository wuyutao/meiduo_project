carts = {
    1: {'num': 1, 'selected': True},
    2: {'num': 2, 'selected': True},
    3: {'num': 3, 'selected': True},
}

if 2 in carts:
    carts.pop(2)

for k, v in carts.items():
    print(k, v)