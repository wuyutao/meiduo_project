import json
import pickle

import base64

if __name__ == '__main__':
    # 将cart_str转成bytes,再将bytes转成base64的bytes,最后将bytes转字典
    # cart_dict = pickle.loads(base64.b64decode(cart_str.encode()))

    # 将字典转成bytes,再将bytes转成base64的bytes,最后将bytes转字符串
    # cookie_cart_str = base64.b64encode(pickle.dumps(cart_dict)).decode()


    # data1 = {
    #     'name': 'wuyutao',
    #     'age': 18,
    # }

    # pickle_data = pickle.dumps(data1)
    # print(type(pickle_data), pickle_data)
    # data2 = pickle.loads(pickle_data)
    # print(type(data2), data2)
    #
    # json_data = json.dumps(data1)
    # print(type(json_data), json_data)
    # data3 = json.loads(json_data)
    # print(type(data3), data3)
    #
    # t1 = base64.b64encode(pickle_data)
    # print(t1)
    # t2 = base64.b64decode(t1)
    # print(t2)
    #
    # t3 = base64.b64encode('hello world'.encode())
    # print(t3)
    #
    # t4 = base64.b64decode(t3)
    # print(t4.decode())

    data1 = {
        'name': 'wuyutao',
        'age': 18,
    }

    t1 = base64.b64encode(pickle.dumps(data1)).decode() # 保存到cookie中的数据
    print(t1)
    t2 = pickle.loads(base64.b64decode(t1.encode())) # 取出cookie数据还原成dict
    print(t2)


