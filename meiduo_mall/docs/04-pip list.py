# -*- coding: utf-8 -*-

# pip install django==1.11.11
# pip install Jinja2
# pip install PyMySQL
# pip install django-redis
# pip install Pillow
# pip install -U Celery
# pip install QQLoginTool
# pip install itsdangerous
# pip install fdfs_client-py-master.zip
# pip install mutagen
# pip install requests
# pip install django-haystack
# pip install elasticsearch==2.4.1
# pip install python-alipay-sdk --upgrade
# pip install django-crontab
# pip install djangorestframework
# pip install djangorestframework-jwt