# -*- coding: utf-8 -*-

'''
通过阅读源代码，发现login函数调用后会更新数据库中的last_login字段
login函数位于django/contrib/auth/__init__.py文件中
login函数最后会通过发出一个信号user_logged_in，来触发更新last_login字段的代码逻辑
更新last_login字段的代码逻辑位于django/contrib/auth/models.py文件中

正常在美多商城登陆发起一个POST请求，处理登陆的代码位于apps/users/views.LoginView
验证用户名密码后会通过login来实现状态保持，由于调用了auth中的login，所以会更新数据库中的last_login字段

美多后台的框架是rest_framework，在配置文件中指定了默认的认证类JWT
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication', # JWT认证即通过token来认证
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
}
由于JWT并没有处理更新last_login的逻辑，所以后台登陆用户，last_login为Null
'''
