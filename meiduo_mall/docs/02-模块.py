# -*- coding: utf-8 -*-

# apps
    # carts 购物车
    # contents 首页广告
    # goods 商品
    # meiduo_admin 管理后台
    # oauth 第三方登陆
    # orders 订单
    # payments 支付
    # users 用户
    # verifications 验证码

# libs 第三方库
    # alipay 支付宝支付
    # captcha 生成图形验证码
    # yuntongxun 容联云通讯

# utils 工具
    # authenticate      自定义用户认证支持用户名密码登陆，以及区分(商城/后台)登陆
    # breadcrumb        面包屑导航，输入三级分类，输出"一级>二级>三级"数据结构
    # categories        生成三级分类联动数据结构
    # db_router         数据库路由，配置mysql主从的时候用到，用来指明哪个数据库配置是写哪个是读
    # jinja2_env        模板引擎Jinja2配置，在这里可以自定义过滤器
    # login             把login_required装饰器封装成了LoginRequiredMixin
    # meiduo_json       字典-加密字符串 用户cookie保存购物车
    # meiduo_signature  QQ加密和解密openId
    # models            模型类基类
    # response_code     respone状态码