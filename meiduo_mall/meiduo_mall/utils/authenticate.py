from django.contrib.auth.backends import ModelBackend
import re
from users.models import User


class MeiduoModelBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        # 变量username的值，可以是用户名，也可以是手机号，需要判断，再查询

        if request is None:
            try:
                user = User.objects.get(username=username, is_superuser=True, is_staff=True)
            except:
                # 如果未查到数据，则返回None，用于后续判断
                try:
                    user = User.objects.get(mobile=username, is_superuser=True, is_staff=True)
                except:
                    return None
        else:
            try:
                user = User.objects.get(username=username)
            except:
                # 如果未查到数据，则返回None，用于后续判断
                try:
                    user = User.objects.get(mobile=username)
                except:
                    return None

        # 判断密码
        if user.check_password(password):
            return user
        else:
            return None
