from goods.models import GoodsChannel


def get_breadcrumb(cat3):
    cat2 = cat3.parent
    cat1 = cat2.parent
    result = {
        'cat1':{
            'url': cat1.channels.all()[0].url,
            'name': cat1.name
        },
        'cat2': cat2,
        'cat3': cat3
    }

    return result
