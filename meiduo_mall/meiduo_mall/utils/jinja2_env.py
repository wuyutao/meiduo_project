from django.template.defaultfilters import safe
from jinja2 import Environment
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse


def jinja2_environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
    })

    # 将自定义的过滤器添加到环境中
    env.filters['p_default'] = p_default

    return env


"""
确保可以使用模板引擎中的{{ url('') }} {{ static('') }}这类语句 
"""


# 自定义过滤器
def p_default(value, arg):
    return value or arg
