from collections import OrderedDict

from goods.models import GoodsChannel

'''
数据结构:
categories = {
    1:{
        'sub1':[
            {'name': '手机','url': '#'},
            {'name': '相机','url': '#'},
            {'name': '数码','url': '#'},
        ],
        'sub2':[
            {'name':'手机通讯', 'sub3':[{'name':'手机', 'url':'#'}, {'name':'对讲机', 'url':'#'}, {'name':'老人机', 'url':'#'}]},
            {'name':'手机配件', 'sub3':[]},
            {'name':'摄影摄像', 'sub3':[]},
        ]
    },
}
'''
def get_categories():
    categories = OrderedDict()

    channels = GoodsChannel.objects.all().order_by('group_id', 'sequence')

    for channel in channels:
        group_id = channel.group_id

        if group_id not in categories:
            categories[group_id] = {
                'sub1': [],
                'sub2': [],
            }

        s1 = channel.category

        # calc sub1
        categories[group_id]['sub1'].append({
            'name': s1.name,
            'url': channel.url
        })

        # calc sub2
        for s2 in s1.subs.all():
            s3_list = []
            for s3 in s2.subs.all():
                s3_list.append(s3)

            categories[group_id]['sub2'].append({
                'name': s2.name,
                'sub3': s3_list,
            })

    return categories
