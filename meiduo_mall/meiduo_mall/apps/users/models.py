from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
from meiduo_mall.utils.models import BaseModel


class User(AbstractUser):
    """自定义用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    email_active = models.BooleanField(default=False, verbose_name='邮箱验证状态')
    default_address = models.ForeignKey('Address', related_name='users', null=True, blank=True,
                                        on_delete=models.SET_NULL, verbose_name='默认地址')

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username


class Area(models.Model):
    """省市区"""
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区'
        verbose_name_plural = '省市区'

    def __str__(self):
        return self.name


class Address(BaseModel):
    # 关联用户
    user = models.ForeignKey(User, related_name='addresses')
    # 标题
    title = models.CharField(max_length=10, null=True)
    # 收件人
    receiver = models.CharField(max_length=10)
    # 省
    province = models.ForeignKey(Area, related_name='provinces')
    # 市
    city = models.ForeignKey(Area, related_name='citys')
    # 区县
    district = models.ForeignKey(Area, related_name='districts')
    # 详细地址
    detail_address = models.CharField(max_length=100)
    # 手机号
    mobile = models.CharField(max_length=11)
    # 固定电话
    phone = models.CharField(max_length=20)
    # 邮箱
    email = models.CharField(max_length=50)
    # 逻辑删除
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'tb_addresses'
        ordering = ['-update_time']
