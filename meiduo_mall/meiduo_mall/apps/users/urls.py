from django.conf.urls import url

from . import views

urlpatterns = [
    # 注册
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    # 登陆
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    # 退出登陆
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    # 用户中心
    url(r'^info/$', views.UserInfoView.as_view(), name='info'),
    # 验证用户名是否存在
    url(r'^usernames/(?P<username>[a-zA-Z0-9_-]{5,20})/count/$', views.UserNameCountView.as_view()),
    # 验证手机号是否存在
    url(r'^mobiles/(?P<mobile>1[345789]\d{9})/count/$', views.UserMobileCountView.as_view()),
    # 保存用户邮箱
    url(r'^emails/$', views.EmailView.as_view()),
    # 验证邮箱
    url(r'^emails/verification/$', views.VerifyEmailView.as_view()),
    # 收货地址
    url(r'^addresses/create/$', views.CreateAddressView.as_view()), # 新增收货地址
    url(r'^addresses/$', views.AddressView.as_view(), name='addresses'), # 收货地址列表
    url(r'^addresses/(?P<address_id>\d+)/$', views.AddressView.as_view()),  # 删除收货地址，修改收货地址
    url(r'^addresses/(?P<address_id>\d+)/default/$', views.AddressDefaultView.as_view()),  # 设为默认收货地址
    url(r'^addresses/(?P<address_id>\d+)/title/$', views.AddressTitleView.as_view()),  # 设置标题
    url(r'^areas/$', views.AreasView.as_view()), # 省市区三级联动
    # 修改密码
    url(r'^password/$', views.ChangePasswordView.as_view(), name='password'),
]