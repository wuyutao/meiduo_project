from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^carts/$', views.AddCart.as_view(), name='mycart'),
    url(r'^carts/simple/$', views.CardsSimple.as_view()),
    url(r'^carts/selection/$', views.CardSelectionView.as_view()),
]