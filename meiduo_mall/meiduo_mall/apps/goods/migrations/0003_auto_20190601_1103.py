# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-06-01 03:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0002_auto_20190531_1534'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sku',
            old_name='default_image',
            new_name='default_image_url',
        ),
    ]
