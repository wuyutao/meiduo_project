# 图片验证码在redis中的过期时间，单位是秒
IMAGE_CODE_REDIS_EXPIRES = 60 * 3
# 短信验证码在redis中的过期时间，单位是秒
SMS_CODE_REDIS_EXPIRES = 60 * 3
# 短信模板id
SEND_SMS_TEMPLATE_ID = 1
# 发送短信验证码间隔
SEND_SMS_CODE_INTERVAL = 60 * 1