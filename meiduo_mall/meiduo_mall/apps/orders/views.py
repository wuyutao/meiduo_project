import json
from decimal import Decimal

from django.http import JsonResponse, HttpResponseNotFound
from django.shortcuts import render

# Create your views here.
from django.views import View
from django.views.generic.base import logger

from goods.models import SKU
from meiduo_mall.utils.login import LoginRequiredMixin
from meiduo_mall.utils.response_code import RETCODE
from orders import constants
from orders.models import OrderInfo, OrderGoods
from users.models import User, Address
from django_redis import get_redis_connection
import time
from django.db import transaction
from django.core.paginator import Paginator, EmptyPage


class OrderSettlementView(LoginRequiredMixin, View):

    def get(self, request):
        addresses = request.user.addresses.filter(is_delete=False)

        redis = get_redis_connection('carts')
        data = redis.hgetall('carts:%d' % request.user.id)
        data = {int(k): int(v) for k, v in data.items()}
        selected = redis.smembers('selected:%d' % request.user.id)
        selected = [int(s) for s in selected]

        skus = []
        total_count = 0
        total_amount = Decimal(0.00)

        freight = Decimal('10.00')
        for sku_id, num in data.items():
            if sku_id in selected:
                sku = SKU.objects.get(id=sku_id)
                sku.num = num
                total_count += num
                total_amount += sku.price * num
                skus.append(sku)


        context = {
            'addresses': addresses,
            'skus': skus,
            'total_count': total_count,  # 总件数
            'total_amount': total_amount,  # 总金额
            'freight': freight,  # 运费
            'payment_amount': total_amount + freight,  # 实付款
        }
        return render(request, 'place_order.html', context)


class OrderCommitView(LoginRequiredMixin, View):
    """订单提交"""

    def post(self, request):
        """保存订单信息和订单商品信息"""

        # 接收
        obj = json.loads(request.body.decode())
        address_id = obj.get('address_id')
        pay_method = obj.get('pay_method')
        if not all([address_id, pay_method]):
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '参数错误'})

        # 验证
        try:
            address = Address.objects.get(id=address_id)
        except:
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '收货地址错误'})

        if pay_method not in OrderInfo.PAY_METHODS_ENUM.values():
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '支付方式不支持'})

        # 显式的开启一个事务
        with transaction.atomic():

            # 创建事务保存点
            save_id = transaction.savepoint()

            try:
                # 处理 - 保存订单基本信息
                order_id = time.strftime('%Y%m%d%H%S%M', time.localtime()) + '%09d' % request.user.id  # 订单号
                order = OrderInfo.objects.create(
                    order_id=order_id,  # 订单号
                    total_count=0,  # 一共多少件商品
                    total_amount=0,  # 总金额
                    freight=Decimal('10.00'),  # 运费
                    pay_method=pay_method,  # 支付方式
                    status=OrderInfo.ORDER_STATUS_ENUM['UNSEND'] if pay_method == OrderInfo.PAY_METHODS_ENUM[
                        'CASH'] else OrderInfo.ORDER_STATUS_ENUM['UNPAID'],  # 订单状态
                    address=address,  # 收货地址
                    user=request.user,  # 用户id
                )

                # 处理 - 保存订单详细信息
                redis = get_redis_connection('carts')
                data = redis.hgetall('carts:%d' % request.user.id)
                data = {int(k): int(v) for k, v in data.items()}
                selected = redis.smembers('selected:%d' % request.user.id)
                selected = [int(s) for s in selected]

                for sku_id, num in data.items():
                    if sku_id in selected:
                        sku = SKU.objects.get(id=sku_id)

                        stock = sku.stock  # 库存
                        sales = sku.sales  # 销量

                        if num > stock:
                            # 库存不足，回滚事务
                            transaction.savepoint_rollback(save_id)
                            return JsonResponse({'code': RETCODE.STOCKERR, 'errmsg': '库存不足'})

                        # sku减库存，增加销量
                        # sku.stock -= num # 直接减少库存的解决方案无法解决并发问题
                        # sku.sales += num
                        # sku.save()
                        # sku改库存，增加销量
                        row = SKU.objects.filter(id=sku.id, stock=stock).update(stock=stock - num, sales=sales + num) # 乐观锁的解决方案
                        if row == 0:
                            transaction.savepoint_rollback(save_id)
                            return JsonResponse({'code': RETCODE.STOCKERR, 'errmsg': '服务器忙，请稍后再试'})

                        # 插入订单详情表
                        OrderGoods.objects.create(
                            order=order,  # 订单
                            sku=sku,  # 商品
                            count=num,  # 数量
                            price=sku.price,  # 单价
                        )

                        # spu增加销量
                        spu = sku.spu
                        spu.sales += num
                        spu.save()

                        # 统计商品总件数和总金额
                        order.total_count += num
                        order.total_amount += sku.price * num

            except Exception as e:
                logger.error(e)
                # 下单失败回滚事务
                transaction.savepoint_rollback(save_id)
                return JsonResponse({'code': RETCODE.DBERR, 'errmsg': '下单失败'})

            # 下单成功提交事务
            transaction.savepoint_commit(save_id)

        # 添加邮费和保存订单信息
        order.total_amount += order.freight
        order.save()

        # 清除购物车中已结算的商品
        pl = redis.pipeline()
        pl.hdel('carts:%s' % request.user.id, *selected)
        pl.srem('selected:%s' % request.user.id, *selected)
        pl.execute()

        # 响应
        return JsonResponse({'code': RETCODE.OK, 'errmsg': '下单成功', 'order_id': order_id})


class OrderSuccessView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, 'order_success.html')


class UserOrderInfoView(LoginRequiredMixin, View):
    """我的订单"""

    def get(self, request, page_num=1):
        """提供我的订单页面"""

        orders = OrderInfo.objects.filter(user=request.user).order_by('-create_time')
        for order in orders:
            amount = 0
            for good in order.skus.all():
                amount += good.price * good.count
            order.amount = amount # 计算订单总金额

        # 分页
        page_num = int(page_num)
        try:
            paginator = Paginator(orders, constants.ORDERS_LIST_LIMIT)
            orders = paginator.page(page_num)
            total_page = paginator.num_pages
        except EmptyPage:
            return HttpResponseNotFound('订单不存在')

        context = {
            'orders': orders,
            'total_page': total_page,
            'page_num': page_num,
        }

        return render(request, 'user_center_order.html', context)


class OrderCommentView(LoginRequiredMixin, View):

    def get(self, request):
        order_id = request.GET.get('order_id')

        goods = OrderGoods.objects.filter(order_id=order_id, is_commented=False)

        skus = []
        for g in goods:
            skus.append({
                'order_id': order_id,
                'sku_id': g.sku_id,
                'default_image_url': g.sku.default_image.url,
                'name': g.sku.name,
                'price': str(g.price),
            })

        context = {
            'skus': skus
        }
        return render(request, 'goods_judge.html', context)

    def post(self, request):
        # 接收
        data = json.loads(request.body.decode())
        order_id = data.get('order_id')
        sku_id = data.get('sku_id')
        comment = data.get('comment')
        score = data.get('score')
        is_anonymous = data.get('is_anonymous')

        # 验证
        if not all([order_id, sku_id, comment, score]):
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '参数错误'})

        if not isinstance(is_anonymous, bool):
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '匿名评价参数错误'})

        scores = [t[0] for t in OrderGoods.SCORE_CHOICES]
        if score not in scores:
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '匿名评价参数错误'})

        try:
            good = OrderGoods.objects.get(order_id=order_id, sku_id=sku_id)
        except:
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '商品未找到'})

        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code': RETCODE.PARAMERR, 'errmsg': '参数sku_id错误'})

        # 处理
        good.score = score
        good.comment = comment
        good.is_anonymous = is_anonymous
        good.is_commented = True
        good.save()

        # 累计评论数据
        sku.comments += 1
        sku.save()
        sku.spu.comments += 1
        sku.spu.save()

        # 查询订单中未评价商品的数量
        good_uncomment_count = OrderGoods.objects.filter(order_id=order_id, is_commented=0).count()

        # 如果没有待评价的商品，设置订单的状态为完成
        if good_uncomment_count == 0:
            OrderInfo.objects.filter(order_id=order_id).update(status=OrderInfo.ORDER_STATUS_ENUM['FINISHED'])

        return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK'})


class CommentSKUView(View):

    def get(self, request, sku_id):
        goods_comment_list = []
        goods = OrderGoods.objects.filter(sku_id=sku_id)
        for g in goods:
            goods_comment_list.append({
                'score': g.score,
                'comment': g.comment,
                'name': '******' if g.is_anonymous else g.order.user.username
            })
        return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'goods_comment_list': goods_comment_list})
