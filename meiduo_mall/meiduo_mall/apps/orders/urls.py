from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^orders/settlement/$', views.OrderSettlementView.as_view(), name='settlement'),
    url(r'^orders/commit/$', views.OrderCommitView.as_view()),
    url(r'^orders/success/$', views.OrderSuccessView.as_view()),
    url(r'^orders/info/$', views.UserOrderInfoView.as_view(), name='info'),
    url(r'^orders/info/(?P<page_num>\d+)/$', views.UserOrderInfoView.as_view()),
    url(r'^orders/comment/$', views.OrderCommentView.as_view()),
    url(r'^comment/(?P<sku_id>\d+)/$', views.CommentSKUView.as_view()),
]