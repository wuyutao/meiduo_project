from django.shortcuts import render
from django.views.generic.base import View
from contents.models import ContentCategory
from meiduo_mall.utils.categories import get_categories


class IndexView(View):
    def get(self, request):
        """提供首页广告界面"""

        contents = {}
        content_category = ContentCategory.objects.all()
        for cat in content_category:
            contents[cat.key] = cat.contents.filter(status=True).order_by('sequence')

        context = {
            'categories': get_categories(), # 查询商品频道和分类
            'contents': contents # 查询广告数据
        }

        return render(request, 'index.html', context)





