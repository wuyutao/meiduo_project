from django.shortcuts import render
from contents.models import ContentCategory
from meiduo_mall.utils.categories import get_categories
from django.conf import settings
import os


def generate_static_index_html():
    contents = {}
    content_category = ContentCategory.objects.all()
    for cat in content_category:
        contents[cat.key] = cat.contents.filter(status=True).order_by('sequence')

    context = {
        'categories': get_categories(),  # 查询商品频道和分类
        'contents': contents  # 查询广告数据
    }

    response = render(None, 'index.html', context)

    path = os.path.join(os.path.join(settings.BASE_DIR, 'static'), 'index.html')

    # 写文件
    with open(path, 'wb') as f:
        f.write(response.content)

