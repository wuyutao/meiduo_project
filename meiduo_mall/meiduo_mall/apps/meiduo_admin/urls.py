# -*- coding: utf-8 -*-

from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from meiduo_admin.views import skus, spus, specs, options, channels, brands, images, orders, permissions
from . import views
from .views import increments
from .views import users

urlpatterns = [
    # JWT
    url(r'^authorizations/$', obtain_jwt_token),


    # 用户总量统计
    url(r'^statistical/total_count/$', increments.UserTotalCountView.as_view()),
    # 日增用户统计
    url(r'^statistical/day_increment/$', increments.UserDayCountView.as_view()),
    # 日活用户统计
    url(r'^statistical/day_active/$', increments.UserActiveCountView.as_view()),
    # 日下单用户量统计
    url(r'^statistical/day_orders/$', increments.UserOrderCountView.as_view()),
    # 月增用户统计
    url(r'^statistical/month_increment/$', increments.UserMonthCountView.as_view()),
    # 日分类商品访问量
    url(r'^statistical/goods_day_views/$', increments.GoodsDayView.as_view()),


    # 用户的查询获取/创建
    url(r'^users/$', users.UserView.as_view()),


    # SKU
    # 获取三级分类信息
    url(r'^skus/categories/$', skus.SKUViewSet.as_view({'get': 'categorie'})),
    # 获取spu表名称数据
    url(r'^goods/simple/$', skus.SKUViewSet.as_view({'get': 'goodssimple'})),
    # 获取spu商品规格信息
    url(r'^goods/(?P<pk>\d+)/specs/$', skus.SKUViewSet.as_view({'get': 'spuspecification'})),


    # SPU
    # 品牌列表
    url(r'^goods/brands/simple/$', spus.SPUViewSet.as_view({'get': 'list_brands'})),
    # 一级分类列表
    url(r'^goods/channel/categories/$', spus.SPUViewSet.as_view({'get': 'list_categories_1'})),
    # 二级三级分类列表
    url(r'^goods/channel/categories/(?P<pk>\d+)/$', spus.SPUViewSet.as_view({'get': 'list_categories_23'})),


    # 规格选项
    # 规格列表
    url(r'^goods/specs/simple/$', options.OptionViewSet.as_view({'get': 'list_spec'})),

    # 频道
    # 频道列表
    url(r'^goods/channel_types/$', channels.ChannelViewSet.as_view({'get': 'list_channel'})),
    # 一级分类
    url(r'^goods/categories/$', channels.ChannelViewSet.as_view({'get': 'list_categories'})),

    # 图片
    url(r'^skus/simple/$', images.ImageViewSet.as_view({'get': 'list_sku'})),

    # 订单
    url(r'^orders/(?P<pk>\d+)/status/$', orders.OrderViewSet.as_view({'put': 'change_status'})),

    # 权限
    url(r'^permission/simple/$', permissions.PermsViewSet.as_view({'get': 'simple'})),
]


# SKU Image
rotue = DefaultRouter()
rotue.register('skus/images', images.ImageViewSet, base_name='images')
urlpatterns += rotue.urls

# SKU
rotue = DefaultRouter()
rotue.register('skus', skus.SKUViewSet, base_name='skus')
urlpatterns += rotue.urls

# 规格管理
rotue = DefaultRouter()
rotue.register('goods/specs', specs.SpecViewSet, base_name='specs')
urlpatterns += rotue.urls

# 频道
rotue = DefaultRouter()
rotue.register('goods/channels', channels.ChannelViewSet, base_name='channels')
urlpatterns += rotue.urls

# 品牌
rotue = DefaultRouter()
rotue.register('goods/brands', brands.BrandViewSet, base_name='brands')
urlpatterns += rotue.urls

# SPU
rotue = DefaultRouter()
rotue.register('goods', spus.SPUViewSet, base_name='spus')
urlpatterns += rotue.urls

# 规格选项
rotue = DefaultRouter()
rotue.register('specs/options', options.OptionViewSet, base_name='options')
urlpatterns += rotue.urls

# 订单
rotue = DefaultRouter()
rotue.register('orders', orders.OrderViewSet, base_name='orders')
urlpatterns += rotue.urls

# 权限
rotue = DefaultRouter()
rotue.register('permission/perms', permissions.PermsViewSet, base_name='perms')
urlpatterns += rotue.urls

# 用户组
rotue = DefaultRouter()
rotue.register('permission/groups', permissions.GroupViewSet, base_name='groups')
urlpatterns += rotue.urls






