# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import Brand


class BrandSerializer(ModelSerializer):
    """
    品牌序列化器
    """

    class Meta:
        model = Brand
        fields = '__all__'







