# -*- coding: utf-8 -*-
from django.contrib.auth.models import Permission, Group
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class PermissionSerializer(ModelSerializer):
    class Meta:
        model = Permission
        fields = ('id', 'name', 'codename', 'content_type')


class GroupSerializer(ModelSerializer):
    # permissions = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Group
        fields = '__all__'

    # def create(self, validated_data):
    #     group = super().create(validated_data)
    #     return group


class PermissionSimpleSerializer(ModelSerializer):
    class Meta:
        model = Permission
        fields = ('id', 'name')


class GroupPermissionSerializer(ModelSerializer):
    class Meta:
        model = Permission
        fields = ('id', 'name')





