# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from rest_framework.settings import api_settings

from goods.models import SKU
from orders.models import OrderInfo, OrderGoods


class SKUSerializer(ModelSerializer):
    class Meta:
        model = SKU
        fields = ('name', 'default_image')


class OrderGoodsSerializer(ModelSerializer):
    sku = SKUSerializer()

    class Meta:
        model = OrderGoods
        fields = ('count', 'price', 'sku')


class OrderSerializer(ModelSerializer):
    """
    订单序列化器
    """
    order_id = serializers.CharField(read_only=True)
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    user = serializers.StringRelatedField()
    skus = OrderGoodsSerializer(many=True)

    class Meta:
        model = OrderInfo
        fields = ('order_id', 'create_time', 'user', 'total_count', 'total_amount', 'freight', 'pay_method', 'status', 'skus')


class OrderStatusSerializer(ModelSerializer):
    order_id = serializers.CharField(read_only=True)
    status = serializers.IntegerField()

    class Meta:
        model = OrderInfo
        fields = ('order_id', 'status')

    def validate_status(self, status):
        if status < 1 or status > 6:
            raise serializers.ValidationError("订单状态码错误")
        return status






