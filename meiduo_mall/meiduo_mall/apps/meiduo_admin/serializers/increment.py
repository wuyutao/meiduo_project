# -*- coding: utf-8 -*-

import rest_framework.serializers
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from goods.models import GoodsVisitCount


class GoodsSerializer(ModelSerializer):
    # 指定返回分类名称
    category = StringRelatedField(read_only=True)

    class Meta:
        model = GoodsVisitCount
        fields = ('count', 'category')
