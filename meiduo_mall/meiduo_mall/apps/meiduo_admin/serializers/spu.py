# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from goods.models import SPU, Brand, GoodsCategory


class SPUSerializer(ModelSerializer):
    """
    SPU表信息序列化器
    """

    brand = serializers.StringRelatedField(read_only=True)
    brand_id = serializers.IntegerField()
    category1_id = serializers.IntegerField()
    category2_id = serializers.IntegerField()
    category3_id = serializers.IntegerField()

    class Meta:
        model = SPU
        fields = (
            'id',
            'name',
            'brand',
            'brand_id',
            'category1_id',
            'category2_id',
            'category3_id',
            'sales',
            'comments',
            'desc_detail',
            'desc_pack',
            'desc_service'
        )


class BrandSerializer(ModelSerializer):
    """
    品牌信息序列化器
    """
    class Meta:
        model = Brand
        fields = ('id','name')


class CategoriesSerializer(ModelSerializer):
    """
    一二三级分类序列化器
    """
    class Meta:
        model = GoodsCategory
        fields = ('id','name')
