# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import SKUImage, SKU


class SKUImageSerializer(ModelSerializer):
    """
    SKU图片序列化器
    """

    class Meta:
        model = SKUImage
        fields = '__all__'


class SKUSerializer(ModelSerializer):
    """
    SKU序列化器
    """

    class Meta:
        model = SKU
        fields = '__all__'







