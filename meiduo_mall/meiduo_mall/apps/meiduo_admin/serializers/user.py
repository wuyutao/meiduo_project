# -*- coding: utf-8 -*-

from rest_framework.serializers import ModelSerializer

from users.models import User


class UserListSerializer(ModelSerializer):
    """
    用户列表序列化器
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'email')


class UserCreateSerializer(ModelSerializer):
    """
    用户创建序列化器
    """
    class Meta:
        model = User
        fields = ('id','username', 'mobile', 'email', 'password')
        extra_kwargs = {
            'username': {
                'max_length': 20,
                'min_length': 5,
            },
            'password': {
                'max_length': 20,
                'min_length': 8,
                'write_only': True
            }
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
