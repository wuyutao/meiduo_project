# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import GoodsChannel, GoodsChannelGroup, GoodsCategory


class ChannelSerializer(ModelSerializer):
    """
    频道序列化器
    """

    category = serializers.StringRelatedField(read_only=True)
    category_id = serializers.IntegerField()
    group = serializers.StringRelatedField(read_only=True)
    group_id = serializers.IntegerField()

    class Meta:
        model = GoodsChannel
        fields = '__all__'


class ChannelGroupSerializer(ModelSerializer):
    """
    频道组序列化器
    """
    class Meta:
        model = GoodsChannelGroup
        fields = '__all__'


class CategorieSerializer(ModelSerializer):
    """
    分类序列化器
    """
    class Meta:
        model = GoodsCategory
        fields = ('id', 'name')





