# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import SPUSpecification


class SpecSerializer(ModelSerializer):
    """
    商品SPU规格序列化器
    """
    spu_id = serializers.IntegerField()
    spu = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = SPUSpecification
        fields = ('id','name','spu_id','spu')
