# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import SKU, SKUSpecification, GoodsCategory, SPU, SPUSpecification, SpecificationOption


class SKUSpecificationSerializer(serializers.ModelSerializer):
    """
    SKU规格表序列化器
    """
    spec_id = serializers.IntegerField(read_only=True)
    option_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = SKUSpecification
        fields = ('spec_id', 'option_id')


class SKUSerializer(ModelSerializer):
    """
    SKU表信息序列化器
    """
    spu_id = serializers.IntegerField()
    spu = serializers.StringRelatedField(read_only=True)
    category_id = serializers.IntegerField()
    category = serializers.StringRelatedField(read_only=True)
    specs = SKUSpecificationSerializer(read_only=True, many=True)

    class Meta:
        model = SKU
        fields = '__all__'

    def create(self, validated_data):
        specs = self.context['request'].data.get('specs')
        sku = SKU.objects.create(**validated_data)
        for spec in specs:
            SKUSpecification.objects.create(sku=sku, spec_id=spec['spec_id'], option_id=spec['option_id'])
        return sku

    def update(self, instance, validated_data):
        specs = self.context['request'].data.get('specs')
        instance.name = validated_data.get('name', instance.name)
        instance.caption = validated_data.get('caption', instance.caption)
        instance.spu_id = validated_data.get('spu_id', instance.spu_id)
        instance.category_id = validated_data.get('category_id', instance.category_id)
        instance.price = validated_data.get('price', instance.price)
        instance.cost_price = validated_data.get('cost_price', instance.cost_price)
        instance.market_price = validated_data.get('market_price', instance.market_price)
        instance.stock = validated_data.get('stock', instance.stock)
        instance.is_launched = validated_data.get('is_launched', instance.is_launched)
        instance.save()
        # 删除之前的选项
        SKUSpecification.objects.filter(sku_id=instance.id).delete()
        # 增加新的选项
        for spec in specs:
            SKUSpecification.objects.create(sku=instance, spec_id=spec['spec_id'], option_id=spec['option_id'])
        return instance


class SKUCategorySerializer(ModelSerializer):
    """
    商品分类序列化器
    """
    class Meta:
        model = GoodsCategory
        fields = '__all__'


class SPUSimpleSerializer(ModelSerializer):
    """
    商品SPU表序列化器
    """
    class Meta:
        model = SPU
        fields = ('id', 'name')


class SPUSpecificationOptionSerializer(ModelSerializer):
    """
    规格选项序列化器
    """
    class Meta:
        model = SpecificationOption
        fields = ('id', 'value')


class SPUSpecificationSerializer(ModelSerializer):
    """
    规格序列化器
    """
    spu = serializers.StringRelatedField(read_only=True)
    spu_id = serializers.IntegerField(read_only=True)
    options = SPUSpecificationOptionSerializer(read_only=True, many=True)

    class Meta:
        model = SPUSpecification
        fields = '__all__'
