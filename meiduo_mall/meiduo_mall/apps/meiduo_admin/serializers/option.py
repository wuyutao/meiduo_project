# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from goods.models import SpecificationOption, SPUSpecification


class OptionSerializer(ModelSerializer):
    """
    规格选项序列化器
    """

    spec = serializers.StringRelatedField(read_only=True)
    spec_id = serializers.IntegerField()

    class Meta:
        model = SpecificationOption
        fields = ('id','value','spec_id','spec')


class SpecSerializer(ModelSerializer):
    """
    规格序列化器
    """

    class Meta:
        model = SPUSpecification
        fields = ('id', 'name')





