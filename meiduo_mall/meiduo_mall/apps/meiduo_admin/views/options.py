# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SpecificationOption, SPUSpecification
from meiduo_admin.serializers.option import OptionSerializer, SpecSerializer
from meiduo_admin.utils import PageNum


class OptionViewSet(ModelViewSet):
    serializer_class = OptionSerializer
    pagination_class = PageNum
    queryset = SpecificationOption.objects.all()

    def list_spec(self, request):
        data = SPUSpecification.objects.all()
        seri = SpecSerializer(data, many=True)
        return Response(seri.data)
