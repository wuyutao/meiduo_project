# -*- coding: utf-8 -*-
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from meiduo_admin.serializers.order import OrderSerializer, OrderStatusSerializer
from meiduo_admin.utils import PageNum
from orders.models import OrderInfo


class OrderViewSet(ModelViewSet):
    serializer_class = OrderSerializer
    pagination_class = PageNum
    queryset = OrderInfo.objects.all()

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword == '' or keyword is None:
            return OrderInfo.objects.all()
        else:
            return OrderInfo.objects.filter(order_id=keyword)

    def change_status(self, request, pk):
        instance = self.get_object()
        seri = OrderStatusSerializer(instance, request.data)
        seri.is_valid(raise_exception=True)
        seri.save()

        return Response(seri.validated_data)
