# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import GoodsChannel, GoodsChannelGroup, GoodsCategory
from meiduo_admin.serializers.channel import ChannelSerializer, ChannelGroupSerializer, CategorieSerializer
from meiduo_admin.utils import PageNum


class ChannelViewSet(ModelViewSet):
    serializer_class = ChannelSerializer
    pagination_class = PageNum
    queryset = GoodsChannel.objects.all().order_by('group_id', 'sequence')

    def list_channel(self, request):
        data = GoodsChannelGroup.objects.all()
        seri = ChannelGroupSerializer(data, many=True)
        return Response(seri.data)

    def list_categories(self, request):
        data = GoodsCategory.objects.filter(parent=None)
        seri = CategorieSerializer(data, many=True)
        return Response(seri.data)
