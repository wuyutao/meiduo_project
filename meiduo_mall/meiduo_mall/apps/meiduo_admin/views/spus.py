# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SPU, Brand, GoodsCategory
from meiduo_admin.serializers.spu import SPUSerializer, BrandSerializer, CategoriesSerializer
from meiduo_admin.utils import PageNum


class SPUViewSet(ModelViewSet):
    serializer_class = SPUSerializer
    pagination_class = PageNum

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword == '' or keyword is None:
            return SPU.objects.all()
        else:
            return SPU.objects.filter(name=keyword)

    def list_brands(self, request):
        data = Brand.objects.all()
        ser = BrandSerializer(data, many=True)
        return Response(ser.data)

    def list_categories_1(self, request):
        data = GoodsCategory.objects.filter(parent=None)
        ser = CategoriesSerializer(data, many=True)
        return Response(ser.data)

    def list_categories_23(self, request, pk):
        data = GoodsCategory.objects.filter(parent=pk)
        ser = CategoriesSerializer(data, many=True)
        return Response(ser.data)
