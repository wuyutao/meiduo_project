# -*- coding: utf-8 -*-
from django.contrib.auth.models import Permission, Group
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from meiduo_admin.serializers.permission import PermissionSerializer, GroupSerializer, PermissionSimpleSerializer
from meiduo_admin.utils import PageNum


class PermsViewSet(ModelViewSet):
    serializer_class = PermissionSerializer
    pagination_class = PageNum
    queryset = Permission.objects.all()

    def simple(self, request):
        data = Permission.objects.all()
        seri = PermissionSimpleSerializer(data, many=True)
        return Response(seri.data)


class GroupViewSet(ModelViewSet):
    serializer_class = GroupSerializer
    pagination_class = PageNum
    queryset = Group.objects.all()

    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     name = request.data.get('name')
    #     permissions_id = request.data.get('permissions')
    #     permissions = []
    #     for pid in permissions_id:
    #         per = Permission.objects.get(id=pid)
    #         permissions.append(per)
    #     group = Group.objects.create(name=name)
    #     group.permissions.set(permissions)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)



