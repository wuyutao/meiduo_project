# -*- coding: utf-8 -*-

from rest_framework.viewsets import ModelViewSet

from goods.models import SPUSpecification
from meiduo_admin.serializers.spec import SpecSerializer
from meiduo_admin.utils import PageNum


class SpecViewSet(ModelViewSet):
    serializer_class = SpecSerializer
    pagination_class = PageNum
    queryset = SPUSpecification.objects.all()
