#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import sys
from django.shortcuts import render


def get_detail_html(sku):
    # 查询商品频道分类
    categories = get_categories()
    # 查询面包屑导航
    breadcrumb = get_breadcrumb(sku.category)

    # 构建当前商品的规格键
    sku_specs = sku.specs.order_by('spec_id')
    sku_key = []
    for spec in sku_specs:
        sku_key.append(spec.option.id)
    # 获取当前商品的所有SKU
    skus = sku.spu.skus.all()
    # 构建不同规格参数（选项）的sku字典
    spec_sku_map = {}
    for s in skus:
        # 获取sku的规格参数
        s_specs = s.specs.order_by('spec_id')
        # 用于形成规格参数-sku字典的键
        key = []
        for spec in s_specs:
            key.append(spec.option.id)
        # 向规格参数-sku字典添加记录
        spec_sku_map[tuple(key)] = s.id
    # 获取当前商品的规格信息
    goods_specs = sku.spu.specs.order_by('id')
    # 若当前sku的规格信息不完整，则不再继续
    if len(sku_key) < len(goods_specs):
        return
    for index, spec in enumerate(goods_specs):
        # 复制当前sku的规格键
        key = sku_key[:]
        # 该规格的选项
        spec_options = spec.options.all()
        for option in spec_options:
            # 在规格参数sku字典中查找符合当前规格的sku
            key[index] = option.id
            option.sku_id = spec_sku_map.get(tuple(key), 0)  # 如果厂家没生产，黑色+128G的话，匹配不到，给一个默认值0，html里面不生成链接就完了

        spec.spec_options = spec_options

    # 渲染页面
    context = {
        'categories': categories,
        'breadcrumb': breadcrumb,
        'sku': sku,
        'specs': goods_specs,
    }
    response = render(None, 'detail.html', context)
    return response.content


if __name__ == '__main__':
    sys.path.insert(0, '../')
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.prod'
    import django
    django.setup()

    from goods.models import SKU
    from meiduo_mall.utils.breadcrumb import get_breadcrumb
    from meiduo_mall.utils.categories import get_categories

    skus = SKU.objects.all()
    for sku in skus:
        html = get_detail_html(sku)
        # file_name = os.path.join(settings.BASE_DIR, 'static/detail/%d.html' % sku.id)
        file_name = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static/detail/%d.html' % sku.id)
        print(file_name)

        with open(file_name, 'wb') as f:
            f.write(html)

    print('ok')